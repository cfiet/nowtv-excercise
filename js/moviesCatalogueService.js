angular.module('movies')
    .service('moviesCatalogue',  function (catalogue){

        this.all = function() {
            return catalogue;
        }
    })

    .constant('catalogue',[
        {
            "title": "Pirates of the Caribbean",
            "slug": "pirates-caribbean",
            "thumbnail": "img/movies/doge-fat-212.gif"
        },

        {
            "title": "Edward Scissorhands",
            "slug": "edward-scissorhands",
            "thumbnail": "img/movies/doge-gradient-212.gif"
        },

        {
            "title": "Rango",
            "slug": "rango",
            "thumbnail": "img/movies/doge-hat-212.gif"
        },

        {
            "title": "Alice in Wonderland",
            "slug": "alice-in-wonderland",
            "thumbnail": "img/movies/doge-peepers-212.gif"
        },

        {
            "title": "Little Miss Sunshine",
            "slug": "little-miss-sunshine",
            "thumbnail": "img/movies/doge-prizza-212.gif"
        },

        {
            "title": "Sunshine",
            "slug": "sunshine",
            "thumbnail": "img/movies/doge-rainbow-212.gif"
        },

        {
            "title": "Sunshine State",
            "slug": "sunshine-state",
            "thumbnail": "img/movies/doge-shake-212.gif"
        },

        {
            "title": "Eternal Sunshine of the Spotless Mind",
            "slug": "eternal-sunshine-spotless-mind",
            "thumbnail": "img/movies/doge-shake-space-212.gif"
        },

        {
            "title": "Sunshine and Oranges",
            "slug": "sunshine-oranges",
            "thumbnail": "img/movies/doge-sunglasses-212.gif"
        },

        {
            "title": "Tears of the Sun",
            "slug": "tears-sun",
            "thumbnail": "img/movies/doge-wink-212.gif"
        },

        {
            "title": "Rising Sun",
            "slug": "rising-sun",
            "thumbnail": "img/movies/doge-hat-212.gif"
        }
    ]);
