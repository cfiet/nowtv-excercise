(function () {
  "use strict";

  var module = angular.module('movies', []);

  module.factory("moviesService", function (moviesCatalogue) {
    return {
      search: function search(phrase) {
        return moviesCatalogue.all().filter(function (movie) {
          return movie.title.toLowerCase().indexOf(phrase.toLowerCase()) > -1;
        });
      }
    };
  });

  module.controller("moviesSuggestions", function($scope, $location, moviesService) {
    $scope.minInput = 3;
    $scope.phrase = "";
    $scope.results = null;

    $scope.search = function() {
      if($scope.phrase.length < $scope.minInput) {
        $scope.results = null;
        return;
      }

      $scope.results = moviesService.search($scope.phrase);
    }

    $scope.open = function (result) {
      $scope.phrase = "";
      $scope.results = null;
      $location.path("/movies/" + result.slug);
    }
  });


  module.config(function ($routeProvider) {
    $routeProvider.when("/movies", {
      template: "<h1>Movies list</h1>"
    }).when("/movies/:slug", {
      template: "<h1>Movie details</h1><a href=\"#/movies\">Back</a>"
    }).otherwise({
      redirectTo: "/movies"
    });
  });
}());
