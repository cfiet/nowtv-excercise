
describe('movies', function() {
  var suggestions, service, serviceMock, serviceStub;

  beforeEach(function () {
    var movies = module("movies");
  });

  describe("suggestions", function () {
    beforeEach(inject(function($rootScope, $controller, moviesService) {
      suggestions = $rootScope.$new();
      $controller("moviesSuggestions", { $scope: suggestions });
      service = moviesService;
    }));

    afterEach(function () {
      if(serviceMock) {
        serviceMock.verify();
        serviceMock.restore();
      }
      if(serviceStub) {
        serviceStub.restore();
      }
    });

    describe("searching", function() {
      describe("when not previously performed", function () {
        describe("and phrase is under 3 characters", function () {
          beforeEach(function () {
            suggestions.phrase = "aa";
          });

          it("does not invoke service", function () {
            // setup
            serviceMock = sinon.mock(service);
            serviceMock.expects("search").never();

            // act
            suggestions.search();
          });

          it("keeps results nulled", function () {
            // act
            suggestions.search();

            //assert
            expect(suggestions.results).toBe(null);
          });
        });

        describe("and phrase is over 3 characters", function () {
          var phrase = "bye, and thanks for the fish!";

          beforeEach(function () {
            suggestions.phrase = phrase;
          });

          it("invokes service with a phrase", function () {
            // setup
            serviceMock = sinon.mock(service);
            serviceMock.expects("search").withArgs(phrase).once();

            // act
            suggestions.search();
          });

          it("sets results to service result", function () {
            // setup
            var results = [];
            serviceStub = sinon.stub(service, "search");
            serviceStub.returns(results);

            // act
            suggestions.search();

            //assert
            expect(suggestions.results).toBe(results);
          });
        });
      });

      describe('when previously performed', function () {
        beforeEach(function () {
          suggestions.phrase = "shark";
          suggestions.results = [{
            title: "Sharknado"
          }, {
            title: "Killer sharks"
          }];
        });

        describe("and phrase is under 3 characters", function () {
          beforeEach(function () {
            suggestions.phrase = "sh";
          });

          it("does not invoke service", function () {
            // setup
            serviceMock = sinon.mock(service);
            serviceMock.expects("search").never();

            // act
            suggestions.search();
          });

          it("sets results to null", function () {
            // act
            suggestions.search();

            //assert
            expect(suggestions.results).toBe(null);
          });
        });

        describe("and phrase is over 3 characters", function () {
          var phrase = "bye, and thanks for the fish!";

          beforeEach(function () {
            suggestions.phrase = phrase;
          });

          it("invokes service with a phrase", function () {
            // setup
            serviceMock = sinon.mock(service);
            serviceMock.expects("search").withArgs(phrase).once();

            // act
            suggestions.search();
          });

          it("sets results to service result", function () {
            // setup
            var results = [];
            serviceStub = sinon.stub(service, "search");
            serviceStub.returns(results);

            // act
            suggestions.search();

            //assert
            expect(suggestions.results).toBe(results);
          });
        });
      });
    });
  });
});
